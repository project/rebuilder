<?php

declare(strict_types=1);

namespace Drupal\rebuilder\Plugin\Rebuilder;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
// phpcs:disable Drupal.Classes.UnusedUseStatement.UnusedUse
use Drupal\rebuilder\Plugin\Rebuilder\RebuilderBase;
// phpcs:enable Drupal.Classes.UnusedUseStatement.UnusedUse
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Theme info rebuilder plug-in.
 *
 * @Rebuilder(
 *   id           = "theme_info",
 *   title        = @Translation("Theme info"),
 *   description  = @Translation("Rebuilds cached theme <code>.info.yml</code> data from disk."),
 *   aliases      = {
 *     "theme-info",
 *     "themeinfo",
 *   },
 * )
 */
class ThemeInfo extends RebuilderBase {

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache containing active theme info.
   *
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   *   The theme extension list.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The Drupal theme handler service.
   */
  public function __construct(
    array $configuration, string $pluginId, array $pluginDefinition,
    TranslationInterface $stringTranslation,
    protected readonly CacheBackendInterface $cache,
    protected readonly ThemeExtensionList $themeExtensionList,
    protected readonly ThemeHandlerInterface $themeHandler,
  ) {

    parent::__construct(
      $configuration, $pluginId, $pluginDefinition, $stringTranslation,
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration, $pluginId, $pluginDefinition,
  ) {

    return new static(
      $configuration, $pluginId, $pluginDefinition,
      $container->get('string_translation'),
      $container->get('cache.bootstrap'),
      $container->get('extension.list.theme'),
      $container->get('theme_handler'),
    );

  }

  /**
   * Get the bootstrap cache key Drupal stores a given theme's info under.
   *
   * @param string $themeName
   *   The theme machine name.
   *
   * @return string
   *   The theme's cache key.
   */
  protected function getThemeCacheKey(string $themeName): string {
    return 'theme.active_theme.' . $themeName;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Add option to specifiy which theme(s) to rebuild info for? I.e. if
   *   you don't want to rebuild all but just one?
   *
   * @see \Drupal\Core\Theme\ThemeInitialization::getActiveThemeByName()
   *   This is where theme info is cached. Annoyingly, it doesn't set a cache
   *   tag on the cached data so we can't just invalidate a cache tag but
   *   instead have to know the cache key and invalidate that.
   */
  public function rebuild(array $options = []): void {

    $this->cache->invalidateMultiple(\array_map(
      [$this, 'getThemeCacheKey'], \array_keys($this->themeHandler->listInfo()),
    ));

    $this->themeExtensionList->reset();

    $this->setOutput($this->t('Theme info rebuilt.'));

  }

}
