<?php

declare(strict_types=1);

namespace Drupal\rebuilder\Plugin\Rebuilder;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
// phpcs:disable Drupal.Classes.UnusedUseStatement.UnusedUse
use Drupal\rebuilder\Plugin\Rebuilder\RebuilderBase;
use Drupal\rebuilder\PluginManager\RebuilderManagerInterface;
use function method_exists;
// phpcs:enable Drupal.Classes.UnusedUseStatement.UnusedUse
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Library definition rebuilder plug-in.
 *
 * @Rebuilder(
 *   id           = "library",
 *   title        = @Translation("Library"),
 *   description  = @Translation("Rebuilds library definitions."),
 *   aliases      = {
 *     "libraries"
 *   },
 * )
 */
class Library extends RebuilderBase {

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The Drupal library discovery service.
   *
   * @param \Drupal\rebuilder\PluginManager\RebuilderManagerInterface $rebuilderManager
   *   The Rebuilder plug-in manager.
   */
  public function __construct(
    array $configuration, string $pluginId, array $pluginDefinition,
    TranslationInterface $stringTranslation,
    protected readonly LibraryDiscoveryInterface $libraryDiscovery,
    protected readonly RebuilderManagerInterface $rebuilderManager,
  ) {

    parent::__construct(
      $configuration, $pluginId, $pluginDefinition, $stringTranslation,
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration, $pluginId, $pluginDefinition,
  ) {

    return new static(
      $configuration, $pluginId, $pluginDefinition,
      $container->get('string_translation'),
      $container->get('library.discovery'),
      $container->get('plugin.manager.rebuilder'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function rebuild(array $options = []): void {

    // Rebuild theme info before rebuilding libraries because themes can extend
    // and override libraries in their .info.yml files. This ensures any changes
    // there get picked up in the rebuilt libraries.
    $this->rebuilderManager->runRebuilder('theme_info');

    // Drupal core >= 11.1.
    //
    // @see https://www.drupal.org/node/3462970
    //   Change record that LibraryDiscovery class is deprecated.
    if (method_exists($this->libraryDiscovery, 'clear')) {

      $this->libraryDiscovery->clear();

    // Drupal core < 11.1.
    //
    // @todo Remove when minimum core becomes 11.1.
    } else {

      /** @phpstan-ignore-next-line */
      $this->libraryDiscovery->clearCachedDefinitions();

    }


    $this->setOutput($this->t('Library definitions rebuilt.'));

  }

}
